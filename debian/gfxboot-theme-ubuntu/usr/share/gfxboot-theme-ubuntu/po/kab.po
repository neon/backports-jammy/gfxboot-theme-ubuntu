# Kabyle translation for gfxboot-theme-ubuntu
# Copyright (c) 2009 Rosetta Contributors and Canonical Ltd 2009
# This file is distributed under the same license as the gfxboot-theme-ubuntu package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: gfxboot-theme-ubuntu\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2016-04-16 01:46+0000\n"
"PO-Revision-Date: 2020-02-15 17:47+0000\n"
"Last-Translator: tasutlelli17 <Unknown>\n"
"Language-Team: Kabyle <kab@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2020-04-10 15:08+0000\n"
"X-Generator: Launchpad (build 2e26c9bbd21cdca248baaea29aeffb920afcc32a)\n"
"Language: kab\n"

#. ok button label
#. txt_ok
msgid "OK"
msgstr "IH"

#. cancel button label
#. txt_cancel
msgid "Cancel"
msgstr "Sefsex"

#. reboot button label
#. txt_reboot
msgid "Reboot"
msgstr "Ales asenker"

#. continue button label
#. txt_continue
msgid "Continue"
msgstr "Kemmel"

#. txt_bootoptions
msgid "Boot Options"
msgstr "Iɣewwaṛen n usenkar"

#. window title for exit dialog
#. txt_exit_title (see txt_exit_dialog)
msgid "Exiting..."
msgstr "Tuffɣa..."

#. txt_exit_dialog
msgid ""
"You are leaving the graphical boot menu and\n"
"starting the text mode interface."
msgstr ""
"Tettedduḍ ad teffɣeḍ seg umuɣ udlif n usenkar akken \n"
"tebdiḍ daɣen tkeččmeḍ deg ugrudem n uskar aḍris."

#. txt_help
msgid "Help"
msgstr "Tallelt"

#. info box title
#. txt_info_title
msgid "Boot loader"
msgstr "Amsalay n usenker"

#. error box title
#. txt_error_title
msgid "I/O error"
msgstr "Tuccḍa I/O"

#. boot disk change dialog title
#. txt_change_disk_title
msgid "Change Boot Disk"
msgstr "Beddel aḍebsi n usenkar"

#. txt_insert_disk
#, c-format
msgid "Insert boot disk %u."
msgstr "Err aḍebsi n usenkar %u."

#. txt_insert_disk2
#, c-format
msgid ""
"This is boot disk %u.\n"
"Insert boot disk %u."
msgstr ""
"Wagi d aḍebṣi n usenker %u.\n"
"Ger aḍebsi n usenker %u."

#. txt_insert_disk3
#, c-format
msgid ""
"This is not a suitable boot disk.\n"
"Please insert boot disk %u."
msgstr ""
"Wagi mačči d aḍebṣi n usenker.\n"
"Ger ttxil-k aḍebsi n usenker %u."

#. password dialog title
#. txt_password_title
msgid "Password"
msgstr "Awal uffir"

#. Keep the newlines and spaces after ':'!
#. txt_password
msgid ""
"Enter your password:   \n"
"\n"
"\n"
msgstr ""
"Sekcem awal-ik uffir   \n"
"\n"
"\n"

#. dvd warning title
#. txt_dvd_warning_title
msgid "DVD Error"
msgstr "Tuccḍa DVD"

#. txt_dvd_warning2
msgid ""
"This is a two-sided DVD. You have booted from the second side.\n"
"\n"
"Turn the DVD over then continue."
msgstr ""
"Wagi d DVD bu ssin n wudmawen. Tsenkreḍ-t seg udem wis ssin.\n"
"\n"
"Tti DVD sakin kemmel."

#. power off dialog title
#. txt_power_off_title
msgid "Power Off"
msgstr "Sens"

#. txt_power_off
msgid "Halt the system now?"
msgstr "Seḥbes anagraw tura?"

#. txt_password
msgid "Password\n"
msgstr "Awal uffir\n"

#. button label for other/more options
#. txt_other_options
msgid "Other Options"
msgstr "Iɣewwaṛen-nniḍen"

#. label for language selection
#. txt_language
msgid "Language"
msgstr "Tutlayt"

#. label for keymap selection
#. txt_keymap
msgid "Keymap"
msgstr "Taneɣruft n unasiw"

#. label for installation mode selection
#. txt_modes
msgid "Modes"
msgstr "Iskaren"

#. label for modes menu
#. txt_mode_normal
msgid "Normal"
msgstr "Amagnu"

#. label for d-i mode menu
#. txt_expert_mode
msgid "Expert mode"
msgstr "Askar n umusnaw"

#. title for accessibility menu
#. txt_access
msgid "Accessibility"
msgstr "Tanekcumt"

#. label for accessibility menu
#. txt_access_none
msgid "None"
msgstr "Ulac"

#. label for accessibility menu
#. txt_access_v1
msgid "High Contrast"
msgstr "Tanmarit εlayen"

#. label for accessibility menu
#. txt_access_v2
msgid "Magnifier"
msgstr "Tasemɣert"

#. label for accessibility menu
#. txt_access_v3
msgid "Screen Reader"
msgstr "Imeɣri n ugdil"

#. label for accessibility menu
#. txt_access_brltty
msgid "Braille Terminal"
msgstr "Aneftaɣ n Bṛay"

#. label for accessibility menu
#. txt_access_m1
msgid "Keyboard Modifiers"
msgstr "Imbeddilen n unasiw"

#. label for accessibility menu
#. txt_access_m2
msgid "On-Screen Keyboard"
msgstr "Anasiw deg ugdil"

#. label for accessibility menu
#. txt_access_m3
msgid "Motor Difficulties - switch devices"
msgstr "Uguren n uḥ€rrek - tiqeffalin"

#. label for accessibility menu
#. txt_access_all
msgid "Everything"
msgstr "Akk"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_ubuntu
msgid "^Try Ubuntu without installing"
msgstr "^Ɛreḍ Ubuntu war asbeddi"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_kubuntu
msgid "^Try Kubuntu without installing"
msgstr "^Ɛreḍ Kubuntu war asbeddi"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_edubuntu
msgid "^Try Edubuntu without installing"
msgstr "^Ɛreḍ Edubuntu war asbeddi"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_xubuntu
msgid "^Try Xubuntu without installing"
msgstr "^Ɛreḍ Xubuntu war asbeddi"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_ubuntu_mid
msgid "^Try Ubuntu MID without installing"
msgstr "^Ɛreḍ Ubuntu MID war asbeddi"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_ubuntu_netbook
msgid "^Try Ubuntu Netbook without installing"
msgstr "^Ɛreḍ Ubuntu Netbook war asbeddi"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_kubuntu_netbook
msgid "^Try Kubuntu Netbook without installing"
msgstr "^Ɛreḍ Kubuntu Netbook war asbeddi"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_lubuntu
msgid "^Try Lubuntu without installing"
msgstr "^Ɛreḍ Lubuntu war asbeddi"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_ubuntu_gnome
msgid "^Try Ubuntu GNOME without installing"
msgstr "^Ɛreḍ Ubuntu GNOME war asbeddi"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_ubuntu_mate
msgid "^Try Ubuntu MATE without installing"
msgstr "^Ɛreḍ Ubuntu MATE war asbeddi"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_start_kubuntu
msgid "^Start Kubuntu"
msgstr "^Senker Kubuntu"

#. Installation mode.
#. txt_menuitem_driverupdates
msgid "Use driver update disc"
msgstr "Seqdec adebṣi n lqem n unuḍaf"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_text_ubuntu
msgid "^Install Ubuntu in text mode"
msgstr "^Sbedd Ubuntu deg uskar aḍris"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_text_kubuntu
msgid "^Install Kubuntu in text mode"
msgstr "^Sbedd Kubuntu deg uskar aḍris"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_text_edubuntu
msgid "^Install Edubuntu in text mode"
msgstr "^Sbedd Edubuntu deg uskar aḍris"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_text_xubuntu
msgid "^Install Xubuntu in text mode"
msgstr "^Sbedd Xubuntu deg uskar aḍris"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu
msgid "^Install Ubuntu"
msgstr "^Sbedd Ubuntu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_kubuntu
msgid "^Install Kubuntu"
msgstr "^Sbedd Kubuntu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_edubuntu
msgid "^Install Edubuntu"
msgstr "^Sbedd Edubuntu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_xubuntu
msgid "^Install Xubuntu"
msgstr "^Sbedd Xubuntu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu_server
msgid "^Install Ubuntu Server"
msgstr "^Sbedd Ubuntu Server"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu_gnome
msgid "^Install Ubuntu GNOME"
msgstr "^Sbedd Ubuntu GNOME"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu_mate
msgid "^Install Ubuntu MATE"
msgstr "^Sbedd Ubuntu MATE"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_enlist_ubuntu_maas
msgid "^Multiple server install with MAAS"
msgstr "^Asebeddi n ddeqs n yiqeddacen s MAAS"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntustudio
msgid "^Install Ubuntu Studio"
msgstr "^Sbedd Ubuntu Studio"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu_mid
msgid "^Install Ubuntu MID"
msgstr "^Sbedd Ubuntu MID"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu_netbook
msgid "^Install Ubuntu Netbook"
msgstr "^Sbedd Ubuntu Netbook"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_kubuntu_netbook
msgid "^Install Kubuntu Netbook"
msgstr "^Sbedd Kubuntu Netbook"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_lubuntu
msgid "^Install Lubuntu"
msgstr "^Sbedd Lubuntu"

#. Installation mode.
#. txt_menuitem_workstation
msgid "Install a workstation"
msgstr "Sbedd tamacint n uxeddim"

#. Installation mode.
#. txt_menuitem_server
msgid "Install a server"
msgstr "Sbedd aqeddac"

#. Installation mode.
#. txt_menuitem_oem
msgid "OEM install (for manufacturers)"
msgstr "Sbedd OEM (i yimfuras)"

#. Installation mode.
#. txt_menuitem_lamp
msgid "Install a LAMP server"
msgstr "Sbedd aqeddac LAMP"

#. Installation mode.
#. txt_menuitem_ltsp
msgid "Install an LTSP server"
msgstr "Sbedd aqeddac LTSP"

#. Installation mode.
#. txt_menuitem_ltsp_mythbuntu
msgid "Install a Diskless Image Server"
msgstr "Sbedd tugna n unagraw war aḍebsi"

#. Installation mode.
#. txt_menuitem_cli
msgid "Install a command-line system"
msgstr "Sbedd anagraw n yizirig n tladna"

#. Installation mode.
#. txt_menuitem_minimal
msgid "Install a minimal system"
msgstr "Sbedd anagraw adday"

#. Installation mode.
#. txt_menuitem_minimalvm
msgid "Install a minimal virtual machine"
msgstr "Sbedd tamacint tuhlist taddayt"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_check
msgid "^Check disc for defects"
msgstr "^Senqed adebṣi ma yexseṛ"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_rescue
msgid "^Rescue a broken system"
msgstr "^Err-d anagraw yeṛṛzen"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_memtest
msgid "Test ^memory"
msgstr "Sekyed ^takatut"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_hd
msgid "^Boot from first hard disk"
msgstr "^Senker seg udebṣi aɣaran amezwaru"

#. Boot option.
#. txt_option_free
msgid "Free software only"
msgstr "Aseɣzan kan ilelli"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_dell_factory_recovery
msgid "^Dell Automatic Reinstall"
msgstr "^Kkes tulsa n usebeddi awurman"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_mythbuntu
msgid "^Install Mythbuntu"
msgstr "^Sbedd Mythbuntu"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_mythbuntu
msgid "^Try Mythbuntu without installing"
msgstr "^Ɛreḍ Mythbuntu war asbeddi"

#. txt_menuitem_live_ubuntukylin
msgid "^Try Ubuntu Kylin without installing"
msgstr "^Ɛreḍ Ubuntu Kylin war asbeddi"

#. txt_menuitem_live_ubuntukylin_netbook
msgid "^Try Ubuntu Kylin Netbook without installing"
msgstr "^Ɛreḍ Ubuntu Kylin Netbook war asbeddi"

#. txt_menuitem_install_text_ubuntukylin
msgid "^Install Ubuntu Kylin in text mode"
msgstr "^Sbedd Kubuntu Kylin deg uskar aḍris"

#. txt_menuitem_install_ubuntukylin
msgid "^Install Ubuntu Kylin"
msgstr "^Sbedd Ubuntu Kylin"

#. txt_menuitem_install_ubuntukylin_netbook
msgid "^Install Ubuntu Kylin Netbook"
msgstr "^Sbedd Ubuntu Kylin Netbook"

#. txt_menuitem_start_ubuntukylin
msgid "^Start Ubuntu Kylin"
msgstr "^Senker Ubuntu Kylin"

#. txt_menuitem_live_ubuntustudio
msgid "^Try Ubuntu Studio without installing"
msgstr "^Ɛreḍ Ubuntu Studio war asbeddi"

#. txt_install_ubuntu_safegraphics
msgid "^Install Ubuntu (safe graphics)"
msgstr ""

#. txt_install_edubuntu_safegraphics
msgid "^Install Edubuntu (safe graphics)"
msgstr ""

#. txt_install_kubuntu_safegraphics
msgid "^Install Kubuntu (safe graphics)"
msgstr ""

#. txt_install_xubuntu_safegraphics
msgid "^Install Xubuntu (safe graphics)"
msgstr ""

#. txt_install_ubuntu_mid_safegraphics
msgid "^Install Ubuntu MID (safe graphics)"
msgstr ""

#. txt_install_ubuntu_netbook_safegraphics
msgid "^Install Ubuntu Netbook (safe graphics)"
msgstr ""

#. txt_install_kubuntu_netbook_safegraphics
msgid "^Install Kubuntu Netbook (safe graphics)"
msgstr ""

#. txt_install_lubuntu_safegraphics
msgid "^Install Lubuntu (safe graphics)"
msgstr ""

#. txt_install_ubuntu_gnome_safegraphics
msgid "^Install Ubuntu GNOME (safe graphics)"
msgstr ""

#. txt_install_ubuntu_mate_safegraphics
msgid "^Install Ubuntu MATE (safe graphics)"
msgstr ""

#. txt_install_mythbuntu_safegraphics
msgid "^Install Mythbuntu (safe graphics)"
msgstr ""

#. txt_install_ubuntu_kylin_safegraphics
msgid "^Install Ubuntu Kylin (safe graphics)"
msgstr ""

#. txt_install_ubuntu_kylin_netbook_safegraphics
msgid "^Install Ubuntu Kylin Netbook (safe graphics)"
msgstr ""

#. txt_install_ubuntu_studio_safegraphics
msgid "^Install Ubuntu Studio (safe graphics)"
msgstr ""

#. txt_try_ubuntu_safegraphics
msgid "^Try Ubuntu without installing (safe graphics)"
msgstr ""

#. txt_try_edubuntu_safegraphics
msgid "^Try Edubuntu without installing (safe graphics)"
msgstr ""

#. txt_try_kubuntu_safegraphics
msgid "^Try Kubuntu without installing (safe graphics)"
msgstr ""

#. txt_try_xubuntu_safegraphics
msgid "^Try Xubuntu without installing (safe graphics)"
msgstr ""

#. txt_try_ubuntu_mid_safegraphics
msgid "^Try Ubuntu MID without installing (safe graphics)"
msgstr ""

#. txt_try_ubuntu_netbook_safegraphics
msgid "^Try Ubuntu Netbook without installing (safe graphics)"
msgstr ""

#. txt_try_kubuntu_netbook_safegraphics
msgid "^Try Kubuntu Netbook without installing (safe graphics)"
msgstr ""

#. txt_try_lubuntu_safegraphics
msgid "^Try Lubuntu without installing (safe graphics)"
msgstr ""

#. txt_try_ubuntu_gnome_safegraphics
msgid "^Try Ubuntu GNOME without installing (safe graphics)"
msgstr ""

#. txt_try_ubuntu_mate_safegraphics
msgid "^Try Ubuntu MATE without installing (safe graphics)"
msgstr ""

#. txt_try_mythbuntu_safegraphics
msgid "^Try Mythbuntu without installing (safe graphics)"
msgstr ""

#. txt_try_ubuntu_kylin_safegraphics
msgid "^Try Ubuntu Kylin without installing (safe graphics)"
msgstr ""

#. txt_try_ubuntu_kylin_netbook_safegraphics
msgid "^Try Ubuntu Kylin Netbook without installing (safe graphics)"
msgstr ""

#. txt_try_ubuntu_studio_safegraphics
msgid "^Try Ubuntu Studio without installing (safe graphics)"
msgstr ""
