# Chuvash translation for gfxboot-theme-ubuntu
# Copyright (c) 2009 Rosetta Contributors and Canonical Ltd 2009
# This file is distributed under the same license as the gfxboot-theme-ubuntu package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: gfxboot-theme-ubuntu\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2016-04-16 01:46+0000\n"
"PO-Revision-Date: 2014-03-21 06:56+0000\n"
"Last-Translator: pentile <pentile@ya.ru>\n"
"Language-Team: Chuvash <cv@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2020-04-10 15:08+0000\n"
"X-Generator: Launchpad (build 2e26c9bbd21cdca248baaea29aeffb920afcc32a)\n"

#. ok button label
#. txt_ok
msgid "OK"
msgstr "OK"

#. cancel button label
#. txt_cancel
msgid "Cancel"
msgstr "Пӑрахӑҫла"

#. reboot button label
#. txt_reboot
msgid "Reboot"
msgstr "Сӳнтерсе ҫут"

#. continue button label
#. txt_continue
msgid "Continue"
msgstr "Малалла"

#. txt_bootoptions
msgid "Boot Options"
msgstr ""

#. window title for exit dialog
#. txt_exit_title (see txt_exit_dialog)
msgid "Exiting..."
msgstr "Тухатпӑр..."

#. txt_exit_dialog
msgid ""
"You are leaving the graphical boot menu and\n"
"starting the text mode interface."
msgstr ""

#. txt_help
msgid "Help"
msgstr "Пулӑшу"

#. info box title
#. txt_info_title
msgid "Boot loader"
msgstr ""

#. error box title
#. txt_error_title
msgid "I/O error"
msgstr ""

#. boot disk change dialog title
#. txt_change_disk_title
msgid "Change Boot Disk"
msgstr ""

#. txt_insert_disk
#, c-format
msgid "Insert boot disk %u."
msgstr ""

#. txt_insert_disk2
#, c-format
msgid ""
"This is boot disk %u.\n"
"Insert boot disk %u."
msgstr ""

#. txt_insert_disk3
#, c-format
msgid ""
"This is not a suitable boot disk.\n"
"Please insert boot disk %u."
msgstr ""

#. password dialog title
#. txt_password_title
msgid "Password"
msgstr "Паруль"

#. Keep the newlines and spaces after ':'!
#. txt_password
msgid ""
"Enter your password:   \n"
"\n"
"\n"
msgstr ""
"Сирӗн парульне ҫырӑр:   \n"
"\n"
"\n"

#. dvd warning title
#. txt_dvd_warning_title
msgid "DVD Error"
msgstr "DVD йӑнӑшӗ"

#. txt_dvd_warning2
msgid ""
"This is a two-sided DVD. You have booted from the second side.\n"
"\n"
"Turn the DVD over then continue."
msgstr ""

#. power off dialog title
#. txt_power_off_title
msgid "Power Off"
msgstr "Сӳнтер"

#. txt_power_off
msgid "Halt the system now?"
msgstr "Системӑна халь чармалла-и?"

#. txt_password
msgid "Password\n"
msgstr "Паруль\n"

#. button label for other/more options
#. txt_other_options
msgid "Other Options"
msgstr "Ытти ӗнерлевсем"

#. label for language selection
#. txt_language
msgid "Language"
msgstr "Чӗлхе"

#. label for keymap selection
#. txt_keymap
msgid "Keymap"
msgstr "Сӑрӑм"

#. label for installation mode selection
#. txt_modes
msgid "Modes"
msgstr "Решимсем"

#. label for modes menu
#. txt_mode_normal
msgid "Normal"
msgstr "Виçеллĕ"

#. label for d-i mode menu
#. txt_expert_mode
msgid "Expert mode"
msgstr "Експерт решимӗ"

#. title for accessibility menu
#. txt_access
msgid "Accessibility"
msgstr "Анлӑрах ӗнерлевсем"

#. label for accessibility menu
#. txt_access_none
msgid "None"
msgstr "Çук"

#. label for accessibility menu
#. txt_access_v1
msgid "High Contrast"
msgstr "Вӑйлӑ контраст"

#. label for accessibility menu
#. txt_access_v2
msgid "Magnifier"
msgstr "Луппӑ"

#. label for accessibility menu
#. txt_access_v3
msgid "Screen Reader"
msgstr "Екран вулакан"

#. label for accessibility menu
#. txt_access_brltty
msgid "Braille Terminal"
msgstr ""

#. label for accessibility menu
#. txt_access_m1
msgid "Keyboard Modifiers"
msgstr "Сӑрӑм мутификкаттӑрсем"

#. label for accessibility menu
#. txt_access_m2
msgid "On-Screen Keyboard"
msgstr "Екранҫи сӑрӑм"

#. label for accessibility menu
#. txt_access_m3
msgid "Motor Difficulties - switch devices"
msgstr ""

#. label for accessibility menu
#. txt_access_all
msgid "Everything"
msgstr "Пӗтӗмпех"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_ubuntu
msgid "^Try Ubuntu without installing"
msgstr "^Ubuntu лартмасӑр усӑ курса пӑх"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_kubuntu
msgid "^Try Kubuntu without installing"
msgstr "^Kubuntu лартмасӑр усӑ курса пӑх"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_edubuntu
msgid "^Try Edubuntu without installing"
msgstr "^Edubuntu лартмасӑр усӑ курса пӑх"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_xubuntu
msgid "^Try Xubuntu without installing"
msgstr "^Xubuntu лартмасӑр усӑ курса пӑх"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_ubuntu_mid
msgid "^Try Ubuntu MID without installing"
msgstr "^Ubuntu MID лартмасӑр усӑ курса пӑх"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_ubuntu_netbook
msgid "^Try Ubuntu Netbook without installing"
msgstr "^Ubuntu Netbook лартмасӑр усӑ курса пӑх"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_kubuntu_netbook
msgid "^Try Kubuntu Netbook without installing"
msgstr "^Kubuntu Netbook лартмасӑр усӑ курса пӑх"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_lubuntu
msgid "^Try Lubuntu without installing"
msgstr "^Lubuntu лартмасӑр усӑ курса пӑх"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_ubuntu_gnome
msgid "^Try Ubuntu GNOME without installing"
msgstr "^Ubuntu GNOME лартмасӑр усӑ курса пӑх"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_ubuntu_mate
msgid "^Try Ubuntu MATE without installing"
msgstr "^Ubuntu MATE лартмасӑр усӑ курса пӑх"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_start_kubuntu
msgid "^Start Kubuntu"
msgstr "^Kubuntu ҫут"

#. Installation mode.
#. txt_menuitem_driverupdates
msgid "Use driver update disc"
msgstr ""

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_text_ubuntu
msgid "^Install Ubuntu in text mode"
msgstr ""

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_text_kubuntu
msgid "^Install Kubuntu in text mode"
msgstr ""

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_text_edubuntu
msgid "^Install Edubuntu in text mode"
msgstr ""

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_text_xubuntu
msgid "^Install Xubuntu in text mode"
msgstr ""

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu
msgid "^Install Ubuntu"
msgstr "^Ubuntu ларт"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_kubuntu
msgid "^Install Kubuntu"
msgstr "^Kubuntu ларт"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_edubuntu
msgid "^Install Edubuntu"
msgstr "^Edubuntu ларт"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_xubuntu
msgid "^Install Xubuntu"
msgstr "^Xubuntu ларт"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu_server
msgid "^Install Ubuntu Server"
msgstr "^Ubuntu Server ларт"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu_gnome
msgid "^Install Ubuntu GNOME"
msgstr "^Ubuntu GNOME ларт"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu_mate
msgid "^Install Ubuntu MATE"
msgstr "^Ubuntu MATE ларт"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_enlist_ubuntu_maas
msgid "^Multiple server install with MAAS"
msgstr ""

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntustudio
msgid "^Install Ubuntu Studio"
msgstr "^Ubuntu Studio ларт"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu_mid
msgid "^Install Ubuntu MID"
msgstr "^Ubuntu MID ларт"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_ubuntu_netbook
msgid "^Install Ubuntu Netbook"
msgstr "^Ubuntu Netbook ларт"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_kubuntu_netbook
msgid "^Install Kubuntu Netbook"
msgstr "^Kubuntu Netbook ларт"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_lubuntu
msgid "^Install Lubuntu"
msgstr "^Lubuntu ларт"

#. Installation mode.
#. txt_menuitem_workstation
msgid "Install a workstation"
msgstr ""

#. Installation mode.
#. txt_menuitem_server
msgid "Install a server"
msgstr "Сервӗр ларт"

#. Installation mode.
#. txt_menuitem_oem
msgid "OEM install (for manufacturers)"
msgstr ""

#. Installation mode.
#. txt_menuitem_lamp
msgid "Install a LAMP server"
msgstr "LAMP сервӗра ларт"

#. Installation mode.
#. txt_menuitem_ltsp
msgid "Install an LTSP server"
msgstr "LTSP сервӗра ларт"

#. Installation mode.
#. txt_menuitem_ltsp_mythbuntu
msgid "Install a Diskless Image Server"
msgstr ""

#. Installation mode.
#. txt_menuitem_cli
msgid "Install a command-line system"
msgstr ""

#. Installation mode.
#. txt_menuitem_minimal
msgid "Install a minimal system"
msgstr ""

#. Installation mode.
#. txt_menuitem_minimalvm
msgid "Install a minimal virtual machine"
msgstr ""

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_check
msgid "^Check disc for defects"
msgstr ""

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_rescue
msgid "^Rescue a broken system"
msgstr ""

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_memtest
msgid "Test ^memory"
msgstr "Тест ^вырӑн"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_hd
msgid "^Boot from first hard disk"
msgstr ""

#. Boot option.
#. txt_option_free
msgid "Free software only"
msgstr "Тӳлевсӗр прукрамма хатӗрӗсем ҫеҫ"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_dell_factory_recovery
msgid "^Dell Automatic Reinstall"
msgstr ""

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_install_mythbuntu
msgid "^Install Mythbuntu"
msgstr "^Mythbuntu ларт"

#. Boot menu item; a ^ prefix indicates a unique accelerator key.
#. txt_menuitem_live_mythbuntu
msgid "^Try Mythbuntu without installing"
msgstr "^Mythbuntu лартмасӑр усӑ курса пӑх"

#. txt_menuitem_live_ubuntukylin
msgid "^Try Ubuntu Kylin without installing"
msgstr "^Ubuntu Kylin лартмасӑр усӑ курса пӑх"

#. txt_menuitem_live_ubuntukylin_netbook
msgid "^Try Ubuntu Kylin Netbook without installing"
msgstr "^Ubuntu Kylin Netbook лартмасӑр усӑ курса пӑх"

#. txt_menuitem_install_text_ubuntukylin
msgid "^Install Ubuntu Kylin in text mode"
msgstr ""

#. txt_menuitem_install_ubuntukylin
msgid "^Install Ubuntu Kylin"
msgstr "^Ubuntu Kylin ларт"

#. txt_menuitem_install_ubuntukylin_netbook
msgid "^Install Ubuntu Kylin Netbook"
msgstr "^Ubuntu Kylin Netbook ларт"

#. txt_menuitem_start_ubuntukylin
msgid "^Start Ubuntu Kylin"
msgstr "^Ubuntu Kylin ҫут"

#. txt_menuitem_live_ubuntustudio
msgid "^Try Ubuntu Studio without installing"
msgstr "^Ubuntu Studio лартмасӑр усӑ курса пӑх"

#. txt_install_ubuntu_safegraphics
msgid "^Install Ubuntu (safe graphics)"
msgstr ""

#. txt_install_edubuntu_safegraphics
msgid "^Install Edubuntu (safe graphics)"
msgstr ""

#. txt_install_kubuntu_safegraphics
msgid "^Install Kubuntu (safe graphics)"
msgstr ""

#. txt_install_xubuntu_safegraphics
msgid "^Install Xubuntu (safe graphics)"
msgstr ""

#. txt_install_ubuntu_mid_safegraphics
msgid "^Install Ubuntu MID (safe graphics)"
msgstr ""

#. txt_install_ubuntu_netbook_safegraphics
msgid "^Install Ubuntu Netbook (safe graphics)"
msgstr ""

#. txt_install_kubuntu_netbook_safegraphics
msgid "^Install Kubuntu Netbook (safe graphics)"
msgstr ""

#. txt_install_lubuntu_safegraphics
msgid "^Install Lubuntu (safe graphics)"
msgstr ""

#. txt_install_ubuntu_gnome_safegraphics
msgid "^Install Ubuntu GNOME (safe graphics)"
msgstr ""

#. txt_install_ubuntu_mate_safegraphics
msgid "^Install Ubuntu MATE (safe graphics)"
msgstr ""

#. txt_install_mythbuntu_safegraphics
msgid "^Install Mythbuntu (safe graphics)"
msgstr ""

#. txt_install_ubuntu_kylin_safegraphics
msgid "^Install Ubuntu Kylin (safe graphics)"
msgstr ""

#. txt_install_ubuntu_kylin_netbook_safegraphics
msgid "^Install Ubuntu Kylin Netbook (safe graphics)"
msgstr ""

#. txt_install_ubuntu_studio_safegraphics
msgid "^Install Ubuntu Studio (safe graphics)"
msgstr ""

#. txt_try_ubuntu_safegraphics
msgid "^Try Ubuntu without installing (safe graphics)"
msgstr ""

#. txt_try_edubuntu_safegraphics
msgid "^Try Edubuntu without installing (safe graphics)"
msgstr ""

#. txt_try_kubuntu_safegraphics
msgid "^Try Kubuntu without installing (safe graphics)"
msgstr ""

#. txt_try_xubuntu_safegraphics
msgid "^Try Xubuntu without installing (safe graphics)"
msgstr ""

#. txt_try_ubuntu_mid_safegraphics
msgid "^Try Ubuntu MID without installing (safe graphics)"
msgstr ""

#. txt_try_ubuntu_netbook_safegraphics
msgid "^Try Ubuntu Netbook without installing (safe graphics)"
msgstr ""

#. txt_try_kubuntu_netbook_safegraphics
msgid "^Try Kubuntu Netbook without installing (safe graphics)"
msgstr ""

#. txt_try_lubuntu_safegraphics
msgid "^Try Lubuntu without installing (safe graphics)"
msgstr ""

#. txt_try_ubuntu_gnome_safegraphics
msgid "^Try Ubuntu GNOME without installing (safe graphics)"
msgstr ""

#. txt_try_ubuntu_mate_safegraphics
msgid "^Try Ubuntu MATE without installing (safe graphics)"
msgstr ""

#. txt_try_mythbuntu_safegraphics
msgid "^Try Mythbuntu without installing (safe graphics)"
msgstr ""

#. txt_try_ubuntu_kylin_safegraphics
msgid "^Try Ubuntu Kylin without installing (safe graphics)"
msgstr ""

#. txt_try_ubuntu_kylin_netbook_safegraphics
msgid "^Try Ubuntu Kylin Netbook without installing (safe graphics)"
msgstr ""

#. txt_try_ubuntu_studio_safegraphics
msgid "^Try Ubuntu Studio without installing (safe graphics)"
msgstr ""
